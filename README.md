# Isogenies

Implementation of the SIDH protocol and its Claw and Adaptive attacks.

DISCLAIMER: This project and all its featured source code has been written for EDUCATIONAL AND ACADEMIC PURPOSES and MUST NOT BE USED IN REAL-WORLD CRYPTOGRAPHIC APPLICATIONS. If you need to use SIDH/SIKE in production, use one the reference implementations (https://sike.org/#implementation) or some other tried-and-tested distribution.

If your need is to learn how the SIDH protocol and its attacks work, then this project can be useful to you.

## Sage Docker image

With Docker installed, run `bash run.sh` to build the Sage image and run it, with this folder mapped inside the container. Alternatively, the code runs on Sage 9.0 compiled with Python 3.

## Sage Scripts

Each script in this list has command-line options, which can be read by means of the `-h` flag. Run them with `sage <script>`.

- `exchange.sage`: sample SIDH key exchange.
- `generate_test_case.sage`: create a parameter set for testing.
- `claw.sage`: tries to break SIDH using a claw-finding algorithm. Use flag `-h` to see optional parameters.
- `adaptive.sage`: implementations of the Adaptive attack by Galbraith, Petit, Shian and Ti.

## Parameter sets

The folder `paramsets` contains Sage files with different parameters which cna be used to instantiate the SIDH protocol. All the Sage scripts (except for the one destined to parameter generation) accept the `-e` flag in order to select one of the parameter sets. Examples:

``` 
sage claw.sage -e 5
sage exchange.sage -e SIKEp434
sage adaptive.sage -e 8
``` 

## Classes

- `SIDHParameters` (`sidh.py`). Contains the parameters `ell_A`, `ell_B`, `e_A`, `e_B`, `f`, `p`, `F` and `E_0`.
- `SIDHPrivateKey` (`sidh.py`). Used to generate private keys and to perform a key exchange, given a `SIDHPublicKey`.
- `SIDHPublicKey` (`sidh.py`). Can be serialized and sent for key exchanges.
- `SIDHGraph` (`graph.py`). Perfoms explorations on SIDH graphs for JSON exporting and later visualization.

## Auxiliary files

- `isogenies.py`: quadratic algorithm to compute cyclic isogenies.
- `utils.py`: auxiliary functions for exploring key pairs, generating primes and handling torsion bases.

## Claw algorithm benchmarking

The bash script `test_claw.sh` runs several instances of the claw algorithm to measure its time and memory performance. The resulting measurements are stored in a `logs` folder. A `Makefile` makes the invocation of the test suite easier: a typical session would be made by typing

```
make clean
make test
```

NOTE: this bash script will not work inside the provided Docker image.