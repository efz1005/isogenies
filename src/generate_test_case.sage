import argparse
from sidh import *
from utils import base_prime
from sage.all_cmdline import Integer

"""
Generate parameters for an SIDH protocol instance
"""

# Console arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument("min_e", help="Minimum exponent to be used for the prime 2, default is 10.", type=Integer, default=10)
parser.add_argument("-a", "--ell_A", help="Prime for Alice, default is 2", type=Integer, default=2)
parser.add_argument("-b", "--ell_B", help="Prime for Bob, default is 3", type=Integer, default=3)
parser.add_argument("-f", help="Cofactor f, default is 1", type=Integer, default=1)
args = parser.parse_args()

# Creation of a base prime
p, e_A, e_B = base_prime(args.ell_A, args.ell_B, args.min_e, f=args.f)

# Paramer instantiation
params = SIDHParameters(ell_A=args.ell_A, ell_B=args.ell_B, e_A=e_A, e_B=e_B, f=args.f)

print("""{{
\"params\": {}
}}""".format(params))
