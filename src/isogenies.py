# -*- coding: utf-8 -*-

def cyclic_isogeny(E, P, ell=0, e=0):
    """Computation of an ell^e-degree cyclic isogeny from E with kernel <P>.

    The returned isogeny is a formal composition of e ell-degree isogenies.
    The algorithm used has complexity O(ell*e^2). With the Strategy terminology
    introduced by De Feo, Jao and Plût, this is an isogeny-based strategy.
    
    Args:
        E (EllipticCurve)
        P (EllipticCurvePoint): a point in E with order ell^e
        ell (int): a prime number. Pass this and e to avoid computing P.order()
        e (int): an exponent. Pass this and ell to avoid computing P.order()
    
    Returns:
        An isogeny from E with kernel <P>
    """

    # we could compute the cyclic isogeny even if
    # it wasn't of perfect power order, but the
    # computation won't be efficient (resort to E.isogeny(P))
    if ell*e == 0:
        if not P.order().is_perfect_power() and not is_prime(ell):
            raise

        ell, e = P.order().perfect_power()

    # generate the list of points P * ell^i
    ell_i_P = [P]
    for i in range(e - 1):
        P *= ell
        ell_i_P.append(P)

    # compute the individual ell-isogenies
    phi = E.isogeny(P)
    for i in range(e - 2, -1, -1):
        phi = phi.codomain().isogeny(phi(ell_i_P[i])) * phi

    return phi