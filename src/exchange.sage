from sidh import *
import argparse
from graph import SIDHGraph
from time import process_time as time

"""
Demo of the SIDH key exchange
"""

# Console arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--expnum", help="Experiment to perform (see paramsets), defaults to 0", type=str, default=0)
parser.add_argument("-g", "--graph", help="Save a json file for the Isogeny Explorer", type=str, default="")
parser.add_argument("-d", "--depth", help="Maximum exploration depth when the -g option is enabled", type=int, default=10)
parser.add_argument("-i", "--iters", help="Maximum iteration number when the -g option is enabled", type=int, default=1000)
args = parser.parse_args()

# Load parameters
load(f"paramsets/{args.expnum}.sage")
params = paramset["params"]

print(f"Parameters:\n{params}")

# Alice key creation, with timing
time_alice_key_start = time()
alice_key = SIDHPrivateKey(params)
alice_key.set_role("Alice")
alice_key.generate_key()
alice_public_key = alice_key.public_key
time_alice_key_end = time()

print(f"\nAlice's private key: ({alice_key.m}, {alice_key.n})")
print(f"Alice's public key: \n{alice_public_key}")

# Bob key creation, with timing
time_bob_key_start = time()
bob_key = SIDHPrivateKey(params)
bob_key.set_role("Bob")
bob_key.generate_key()
bob_public_key = bob_key.public_key
time_bob_key_end = time()

print(f"\nBob's private key: ({bob_key.m}, {bob_key.n})")
print(f"Bob's public key: \n{bob_public_key}")

# Key agreement on Alice's side, with timing
time_alice_agreement_start = time()
secret_AB = alice_key.key_agreement(bob_public_key)
time_alice_agreement_end = time()

# Key agreement on Bob's side, with timing
time_bob_agreement_start = time()
secret_BA = bob_key.key_agreement(alice_public_key)
time_bob_agreement_end = time()

print("\n\nTIME")
print(f" Alice key generation: {time_alice_key_end - time_alice_key_start}s")
print(f" Bob key generation: {time_bob_key_end - time_bob_key_start}s")
print(f" Alice key exchange: {time_alice_agreement_end - time_alice_agreement_start}s")
print(f" Bob key exchange: {time_bob_agreement_end - time_bob_agreement_start}s")

print(f"\n\nSuccessful key agreement? {secret_AB == secret_BA}")
print(f"Shared secret: {secret_AB}")

# Graph exploration and storage in a JSON file for later visualization
if len(args.graph):
    print("\nCreating the SIDHGraph...")
    # Create a graph to record the exchange
    graph = SIDHGraph(params, record=(len(args.graph) > 0))

    print(" Exploring the neighbourhood of relevant nodes...")
    # Explore neighbourhood of relevant nodes
    graph.explore_graph(from_node=params.E, max_iter=args.iters, max_depth=args.depth)
    graph.explore_graph(from_node=alice_key.public_key.curve, max_iter=args.iters, max_depth=args.depth)
    graph.explore_graph(from_node=bob_key.public_key.curve, max_iter=args.iters, max_depth=args.depth)
    graph.explore_graph(from_node=bob_key.psi.codomain(), max_iter=args.iters, max_depth=args.depth)

    print(" Adding relevant isogenies...")
    # Add relevant isogenies
    graph.add_isogeny(alice_key.phi, params.ell_A, "A")
    graph.add_isogeny(alice_key.psi, params.ell_A, "A")
    graph.add_isogeny(bob_key.phi, params.ell_B, "B")
    graph.add_isogeny(bob_key.psi, params.ell_B, "B")

    print(" Adding relevant nodes...")
    # Add relevant nodes
    graph.add_node(params.E, "E0")
    graph.add_node(alice_key.public_key.curve, "EA")
    graph.add_node(bob_key.public_key.curve, "EB")
    graph.add_node(secret_AB, "EAB")

    print(" Saving...")
    # Save to file
    f = open(args.graph, "w")
    f.write(str(graph))
    f.close()