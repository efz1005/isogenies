from numpy.random import binomial
from sage.all_cmdline import gcd, sqrt, inverse_mod, is_prime, floor, log, Integer
from sage.misc.prandom import randrange

def all_reduced_keys(ell, e):
    """Generator of the generators of cyclic subgroups
    of $\\Z/ell^e\\Z \\times \\Z/ell^e\\Z$ having order $ell^e$

    Parameters:
        ell (int): prime
        e (int): exponent of ell, greater than 0

    Yields:
        (int, int): the next subgroup generator

    """
    for m in range(ell ** e):
        yield (m, 1)

    for n in range(ell ** (e - 1)):
        yield (1, ell * n)


def random_all_reduced_keys(ell, e, seed=None):
    """Random generator of the generators of cyclic subgroups
    of $\\Z/ell^e\\Z \\times \\Z/ell^e\\Z$ having order $ell^e$

    Parameters:
        ell (int): prime
        e (int): exponent of ell, greater than 0

    Yields:
        (int, int): the next subgroup generator

    """
    if seed is not None:
        random.seed(seed=seed)

    prob_type_0 = ell ** e / (ell ** e + ell ** (e - 1))

    # additive generator for Z/ell ** eZ
    gen_0 = 0
    while gen_0 % ell == 0:
        gen_0 = randrange(0, ell ** e)

    # additive generator for ell * Z/ell ** (e-1)Z
    gen_1 = 0
    while gen_1 % ell == 0:
        gen_1 = randrange(0, ell ** (e-1))

    m = gen_0
    n = gen_1

    yield (0, 1)
    yield (1, 0)

    while m != 0 or n != 0:
        if n == 0 or m != 0 and not binomial(n=1, p=prob_type_0):
            yield (m, 1)
            m = (m + gen_0) % ell ** e
        else:
            yield (1, n*ell)
            n = (n + gen_1) % ell ** (e-1)


def base_prime(ell_A, ell_B, min_e, f=1):
    """Find a prime ell_A^e_A * ell_B^e_B * f - 1 amb e_A >= min_e.

    Args:
        ell_A (int)
        ell_B (int)
        min_e (int)
        f (int)

    Returns:
        int
    """
    done = False

    while not done:
        a = min_e
        b = floor(log(ell_A ** min_e, ell_B))

        for e_A in range(a, a + 5):
            for e_B in range(b, b + 5):
                p = (ell_A ** e_A) * (ell_B ** e_B) * f - 1
                if is_prime(p):
                    done = True
                    break
            if done:
                break

        min_e += 1

    return p, e_A, e_B


def supersingular_number(p):
    """Returns the number of supersingular j-invariants in characteristic p.

    Args:
        p (int)

    Returns:
        int
    """
  
    e_0 = (p % 3) == 2
    e_1728 = (p % 4) == 3
    return floor(p/12.) + e_0 + e_1728


def normalize_m_n(m, n, N):
    """
    Given the pair (m,n) modulo N, with one of m or n coprime to N, invert
    the  invertible component to create a pair (1, n') or (m', 1).

    Args:
        m (int)
        n (int)
        N (int)
    """

    if gcd(m, N) == 1:
        inv = inverse_mod(m, N)
    else:
        inv = inverse_mod(n, N)

    m, n = m * inv, n * inv
    return (m % N, n % N)


def random_m_n(N):
    """Return a random pair modulo N.

    Args:
        N (int)
    """
    m = N
    n = N
    while gcd(m, N) != 1 and gcd(n, N) != 1:
        m = randrange(0, N-1)
        n = randrange(0, N-1)

    return normalize_m_n(m, n, N)


def torsion_basis(E, N, order=0):
    """Find an N-torsion basis of the elliptic curve E.

    Args:
        E (EllipticCurve)
        N (int): must be coprime to the characteristic of the finite field
            over which E is defined.
        order (int): spares computation if the curve order is already known
    """
    if order == 0:
        order = E.order()

    generators = E.objgens()[1]
    P, Q = map(lambda R: Integer(order / N ** 2) * R, generators)

    return P, Q


def select_point(m, P, Q):
    """Selects a point from the pair P, Q depending on the test m==1

    Args:
        m (int)
        P (EllipticCurvePoint)
        Q (EllipticCurvePoint)
    """
    if m == 1:
        return Q
    else:
        return P