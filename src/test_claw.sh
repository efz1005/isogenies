#!/bin/bash

if [ ! -d logs ]; then
	mkdir -p logs;
	mkdir -p logs/mn;
	mkdir -p logs/dfs;
fi

for i in $(seq 0 5);
do
	echo "Starting test no. $i"

	date
	for j in $(seq 1 $1);
	do
		echo "MN test"
		{
		first_output="logs/mn/${i}_claw_${j}.txt"
		first_time="logs/mn/${i}_time_claw_${j}.txt"
		/usr/bin/time -v ~/sage-8.9/sage claw.sage -a MN -e $i > $first_output 2> $first_time
		if [ $? -ne 0 ]; then
		    echo "	TEST ${i}_${j} MN FAIL"
		fi
		echo "$(tail -n 23 $first_time)" > $first_time
		}

		echo "DFS test"
		{
		first_output="logs/dfs/${i}_claw_${j}.txt"
		first_time="logs/dfs/${i}_time_claw_${j}.txt"
		/usr/bin/time -v ~/sage-8.9/sage claw.sage -a DFS -e $i > $first_output 2> $first_time
		if [ $? -ne 0 ]; then
		    echo "	TEST ${i}_${j} DFS FAIL"
		fi
		echo "$(tail -n 23 $first_time)" > $first_time
		}
	done

	date
	echo "Finished test no. $i"
	echo
done