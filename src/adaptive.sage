from sage import *
from utils import normalize_m_n
from isogenies import cyclic_isogeny
from copy import deepcopy
from tqdm import tqdm
import argparse
from graph import *


def adaptive(params, oracle, victim_public_key, attacker_role="Alice", randomize=False, graph=SIDHGraph(None, record=False)):
    """
    Performs the adaptive attack by Galbraith et al. against Bob's public key,
    with a generalization to allow Bob's prime to be odd.

    Args:
        params (SIDHParameters): SIDH protocol parameters
        oracle: function providing bit information about Alice's public key
        victim_public_key (SIDHPublicKey)
        randomize (bool): if True, Bob's private key will change at each
            oracle query. If False, we will reuse the same key each time.
        graph (SIDHGraph): if defined, it stores a graph with the isogenies
            defined by the partial knowledge of Alice's private key.
    """
    print("\nDetermining private key type...")

    attacker_key = SIDHPrivateKey(params)
    attacker_key.set_role(attacker_role)
    attacker_key.generate_key()
    faulty_public_key = deepcopy(attacker_key.public_key)

    if attacker_role == "Alice":
        ell = params.ell_B
        e = params.e_B
    elif attacker_role == "Bob":
        ell = params.ell_A
        e = params.e_A
    else:
        raise

    # Determine if the key is (1, K) or (K, 1)
    R, S = faulty_public_key.auxiliary_points
    R, S = R + (ell ** (e - 1)) * S, S
    faulty_public_key.auxiliary_points = (R, S)

    print(" - Performing key exchange on Alice's side")
    E_AB = attacker_key.key_agreement(victim_public_key)

    print(" - Performing key exchange on Bob's side")
    second_type = oracle(faulty_public_key, E_AB)

    K = 0

    # Loop to get all ell-coefficients of K
    print("\nGetting key coefficients...")
    for i in tqdm(range(e)):
        x = 0
        found_key_coefficient = False

        # Test each value between 0 and ell - 2 (if none is correct,
        # then the value must be ell - 1)
        while x <= ell - 2 and not found_key_coefficient:
            a, b = 1, - (K + ell**i * x) * ell**(e - i - 1)
            c, d = 0, 1 + ell ** (e - i - 1)
            if second_type:
                a, b, c, d = d, c, b, a

            if randomize:
                attacker_key.generate_key()
                faulty_public_key = attacker_key.public_key
            else:
                faulty_public_key = deepcopy(attacker_key.public_key)

            R, S = faulty_public_key.auxiliary_points
            R, S = a * R + b * S, c * R + d * S
            faulty_public_key.auxiliary_points = (R, S)

            E_C = attacker_key.key_agreement(victim_public_key)
            found_key_coefficient = oracle(faulty_public_key, E_AB)

            if found_key_coefficient:
                K += ell**i * x

            x += 1

        if not found_key_coefficient:
            K += ell**i * x

    if second_type:
        return (K, 1)
    else:
        return (1, K)

if __name__ == "__main__":

    # Console arguments parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--expnum", help="Experiment to perform (see paramsets), defaults to 0", type=str, default="0")
    parser.add_argument("-r", "--randomize", help="Randomize malicious key at each iteration", action='store_true', default=False)
    parser.add_argument("-g", "--graph", help="Save a json file for the Isogeny Explorer", type=str, default="")
    parser.add_argument("-d", "--depth", help="Maximum exploration depth when the -g option is enabled", type=int, default=10)
    parser.add_argument("-i", "--iters", help="Maximum iteration number when the -g option is enabled", type=int, default=1000)
    parser.add_argument("-a", "--attacker-role", help="Role played by the attacker: 'Alice' or 'Bob'. Default is Alice.", type=str, default="Alice")
    args = parser.parse_args()

    # Load a parameter set
    load(f"paramsets/{args.expnum}.sage")
    params = paramset["params"]

    # Create a graph to explore and save in a JSON
    graph = SIDHGraph(params, record=(len(args.graph) > 0))
    graph.explore_graph(from_node=params.E, max_iter=args.iters, max_depth=args.depth)
    graph.add_node(params.E, special="E0")

    victim_role = "Bob" if args.attacker_role[0].upper() == "A" else "Alice"
    attacker_role = "Alice" if args.attacker_role[0].upper() == "A" else "Bob"

    print(f"Public data:\n p={params.p}")
    print(f" ell_A={params.ell_A}; e_A={params.e_A}")
    print(f" ell_B={params.ell_B}; e_B={params.e_B}\n")
    print(f"Malicious part is acting as {attacker_role}\n")

    # Generate a random key for Alice to attack it
    victim_key = SIDHPrivateKey(params)
    victim_key.set_role(victim_role)
    victim_key.generate_key()
    victim_public_key = victim_key.public_key

    # Explore graph from Alice's public key
    graph.explore_graph(from_node=victim_public_key.curve, max_iter=args.iters, max_depth=args.depth)
    graph.add_node(victim_public_key.curve, special="EA")

    print("{}'s private key, normalized: \n ({}, {})".format(
        victim_role,
        *normalize_m_n(victim_key.m, victim_key.n, params.ell_A ** params.e_A))
    )


    def oracle(public_key, E_AB):
        """Oracle function for usage in the adaptive attack."""
        return victim_key.key_agreement(public_key) == E_AB


    # Perform attack
    (m, n) = adaptive(params, oracle, victim_public_key, attacker_role=attacker_role, randomize=args.randomize, graph=graph)
    print("\nRecovered private key:")
    print(f" ({m}, {n})")

    # Store graph, if required by the arguments
    if len(args.graph):
        f = open(args.graph, "w")
        f.write(str(graph))
        f.close()