from sidh import *
from isogenies import cyclic_isogeny
from utils import *
from graph import *
import argparse
from tqdm import tqdm
from collections import defaultdict
from time import process_time_ns

def explore_keys(E, basis, ell, f, c):
    """Generator for the MN-exploration of isogenies

    Args:
        E (EllipticCurve): starting curve
        basis: tuple of points generating the ell^f-torsion of E
        ell (int): prime number
        f (int): exponent
        c (int): exponent (f + c = e)

    Yields:
        j, (phi, (m, n)), keys
        j: finite field element with the j-invariant of the codomain of phi
        phi: isogeny between E and an elliptic curve
        (m, n): pair of integers generating phi
        keys: tqdm generator (for clean console-output purposes)
    """
    keys_count = int(ell ^ (f - 1) * (ell + 1))

    keys = tqdm(random_all_reduced_keys(ell, f), total=keys_count)
    for (m, n) in keys:
        R = m * basis[0] + n * basis[1]
        R *= ell ** c

        phi = cyclic_isogeny(E, R, ell, f)
        j = phi.codomain().j_invariant()

        yield j, (phi, (m, n)), keys


def explore_dfs_optimized(E, basis, ell, f, c):
    """Generator for the DFS-exploration of isogenies

    Args:
        E (EllipticCurve): starting curve
        basis: tuple of points generating the ell^f-torsion of E
        ell (int): prime number
        f (int): exponent
        c (int): exponent (f + c = e)

    Yields:
        j, (phi, (m, n)), keys
        j: finite field element with the j-invariant of the codomain of phi
        phi: isogeny between E and an elliptic curve
        (m, n): pair of integers generating phi
        keys: tqdm generator (for clean console-output purposes)
    """
    stack = []

    # Push initial points
    P, Q = ell ** c * basis[0], ell ** c * basis[1]

    keys = list(all_reduced_keys(ell, 1))
    shuffle(keys)
    for (m, n) in keys:
        R = m*P + n*Q
        S = select_point(m, P, Q)
        phi = E.isogeny(ell ** (f - 1) * R)
        stack.append((phi, (m, n), 1, phi(R), phi(S)))

    keys_count = int(ell ** (f - 1) * (ell + 1))
    progress = tqdm(total=keys_count)
    coefs = list(range(ell))
    while len(stack) > 0:
        # Pop stack
        phi, (r, s), d, T, S = stack.pop()

        # Push new points, except the backtracking point
        if d < f:
            S *= ell
            shuffle(coefs)
            for x in coefs:
                m, n = r, s
                if m == 1:
                    n += x  * (ell ** d)
                else:
                    m += x  * (ell ** d)

                R = T + x * S
                psi = phi.codomain().isogeny(ell ** (f - d - 1) * R)
                stack.append((psi * phi, (m, n), d + 1, psi(R), psi(S)))

        # If depth f has been reached, yield
        else:
            progress.update()
            yield phi.codomain().j_invariant(), (phi, (r, s)), progress


def find_kernel_optimized(params, public_key, pairs, psi, ell, f, c):
    """Returns a pair (m, n) with the full attacked public key.
    
    Given an intermediate j-invariant of E_j and isogenies from E_0 and E_A
    to it, deduce the full private key that generated the public key E_A.

    Args:
        params (SIDHParameters): SIDH protocol parameters
        public_key (SIDHPublicKey): public key being attacked
        pairs: list of (phi, (r, s)) pairs, where phi is an isogeny from E_0
            to E_j, and (r, s) are the first f bits of the private key (which 
            yield phi).
        psi: the isogeny from E_j to E_A
        ell (int): a prime number
        f (int): exponent, length of phi
        c (int): exponent, length of psi

    Returns:
        (m, n): pair of integers representing a private key
        S (EllipticCurvePoint): kernel generator based on the m, n coefficients
    """
    e = f + c
    EA = public_key.curve
    aux = public_key.auxiliary_points
    
    j = EA.isomorphism_to(psi.codomain())
    aux = (j(public_key.auxiliary_points[0]), j(public_key.auxiliary_points[1]))

    for phi, (r, s) in pairs:
        i = phi.codomain().isomorphism_to(psi.domain())

        # Check the image of the auxiliary points
        if psi(i(phi(params.P_B))) != aux[0] or psi(i(phi(params.Q_B))) != aux[1]:
            continue

        # Prepare points for computation
        R = r * params.P_A + s * params.Q_A
        R = i(phi(R))
        S = ell ** f * i(phi(select_point(r, params.P_A, params.Q_A)))
        S_ = ell ** (e - f - 1) * S
        k = 0

        # bit-by-bit recovery of missing key part
        for i in tqdm(range(c)):
            order = ell ** (c - i - 1)

            R = psi[i](R)
            S = psi[i](S)
            S_ = psi[i](S_)
            
            x = 0
            found = False
            while x <= ell - 2 and not found:
                found = x * S_ == - order * R
                x += 1

            if found:
                x -= 1
                
            k += x * ell ** (f + i)
            R += x * ell ** i * S

        if r == 1:
            s += k
        else:
            r += k

        return (r, s), r * params.P_A + s * params.Q_A

    return None


def claw(params, public_key, explore_graph=explore_dfs_optimized, **kwargs):
    """Perform a claw-attack on a SIDH public key.

    Args:
        params (SIDHParameters): SIDH protocol parameters
        public_key (SIDHPublicKey): SIDH public key to be attacked
        explore_graph: function for graph exploration
        max_memory (int): the maximum exponent of the dictionary exploration
        graph (SIDHGraph): graph for recording the exploration in a JSON file

    Returns:
        (m, n): pair of integers representing a private key
        S (EllipticCurvePoint): kernel generator based on the m, n coefficients
    """
    if "max_memory" in kwargs:
        max_memory = kwargs["max_memory"]
    else:
        max_memory = 15

    if "graph" in kwargs:
        graph = kwargs["graph"]
    else:
        graph = SIDHGraph(None, record=False)

    ell = params.ell_A
    e = params.e_A
    f = min(floor(e/2.0), max_memory)
    c = e - f

    # Generate a dictionary of j-invariants for E0
    j_invariants = defaultdict(list)
    basis = (params.P_A, params.Q_A)
    E0 = params.E
    print("Exploring from E0 to generate dictionary of j-invariants...")

    dict_start = process_time_ns()
    for j, data, progress in explore_graph(E0, basis, ell, f, c):
        phi = data[0]
        graph.add_isogeny(phi, ell, "B")
        graph.add_node(phi.codomain(), "EB")

        j_invariants[j].append(data)
    progress.close()
    dict_end = process_time_ns()

    dict_time = float((dict_end - dict_start) / 1e6)
    isogeny_time = float(dict_time / float(ell * (ell ** (f-1) + 1)))
    print(f"\nComputation of intermediate j-invariants took {dict_time:.1f} ms ({isogeny_time:.1f} ms/isogeny).")

    EA = public_key.curve
    basis = torsion_basis(EA, ell ** e, order=(params.p + 1)**2)
    print("\nExploring from EA to find a collision...")
    for j, data, progress in explore_graph(EA, basis, ell, c, f):
        if j in j_invariants:
            progress.write("\nFound a collision, checking whether it is proper and computing kernel...")
            # Compute the dual of hatpsi
            hatpsi, (m, n) = data
            kerpsi = (ell ** f) * hatpsi(select_point(m, *basis))
            psi = cyclic_isogeny(kerpsi.curve(), kerpsi, ell, c)

            # Sieve the generated pairs to find a generator
            key = find_kernel_optimized(params, public_key, j_invariants[j], psi, ell, f, c)
            if key is not None:
                progress.close()

                graph.add_node(j, "EAB")
                graph.add_isogeny(psi, ell, "A")

                return key

    raise Exception("Unable to find a match!")


# Console arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument("-a", "--algorithm", help="Choose between the MN and DFS algorithms to explore the isogeny graph (default is DFS)", type=str, default="DFS")
parser.add_argument("-e", "--expnum", help="Experiment to perform (see paramsets), defaults to 0", type=str, default=0)
parser.add_argument("-g", "--graph", help="Save a json file for the Isogeny Visualization Tool", type=str, default="")
parser.add_argument("-d", "--depth", help="Maximum exploration depth when the -g option is enabled", type=int, default=10)
parser.add_argument("-i", "--iters", help="Maximum iteration number when the -g option is enabled", type=int, default=1000)
parser.add_argument("-m", "--max-memory", help="Maximum exponent to store in memory", type=int, default=15)
args = parser.parse_args()

# Load parameters
load(f"paramsets/{args.expnum}.sage")
params = paramset["params"]

# Create a randomized key to attack
private_key = SIDHPrivateKey(params)
private_key.set_role("Alice")
private_key.generate_key()
public_key = private_key.public_key
privkey = (private_key.m, private_key.n)

print(f"Public data:\n p={params.p}")
print(f" ell_A={params.ell_A}; e_A={params.e_A}")
print(f" ell_B={params.ell_B}; e_B={params.e_B}\n")

print(f"Public key: {public_key.curve.j_invariant()}")
R = privkey[0] * params.P_A + privkey[1] * params.Q_A
print(f"Private key to be found: <{privkey[0]}P + {privkey[1]}Q> = <{R}>\n")

# Choose exploration algorithm
if args.algorithm.upper() == "MN":
    explore_graph = explore_keys
else: # DFS or bad input
    explore_graph = explore_dfs_optimized

# Create a graph instance to record the attack in a JSON file
graph = SIDHGraph(params, record=(len(args.graph) > 0))
graph.explore_graph(from_node=params.E, max_iter=args.iters, max_depth=args.depth)
graph.explore_graph(from_node=public_key.curve, max_iter=args.iters, max_depth=args.depth)

# Perform the claw-attack
(m,n), S = claw(params, public_key, explore_graph=explore_graph, graph=graph, max_memory=args.max_memory)

graph.add_node(params.E, "E0")
graph.add_node(public_key.curve, "EA")

# Print the attack results and a discrete logarithm (to show the found point
# is an actual kernel generator)
print(f"\n\nFound private kernel <{m}P + {n}Q> = <{S}>")
dlp = discrete_log(R, S, params.ell_A ** params.e_A, operation='+')
print(f"\n  {R} = {dlp} * {S}\n")

# Save the graph recording into a file
if len(args.graph):
    f = open(args.graph, "w")
    f.write(str(graph))
    f.close()