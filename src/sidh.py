from isogenies import cyclic_isogeny
from utils import random_m_n
from sage.all_cmdline import GF, EllipticCurve, Integer


class SIDHParameters():
    """
    A class with all the parameters of a SIDH protocol instance.

    Attributes:
        p (Integer): Prime number of the form p = ell_A^e_A * ell_B^e_B * f - 1
            See below for meaning of each factor and exponent
        ell_A (Integer): The prime used by Alice (default=2)
        ell_B (Integer): The prime used by Bob (default=3)
        e_A (Integer): The exponent used by Alice, meaning her isogenies will 
            have degree ell_A^e_A (equivalently, they will be the composition
            of e_A ell_A-degree isogenies)
        e_B (Integer): The exponent used by Bob, meaning her isogenies will have
            degree ell_B^e_B (equivalently, they will be the composition
            of e_B ell_B-degree isogenies)
        f (Integer): A cofactor that can be controlled during the generation 
            of p. If {ell_A, ell_B} != {2, 3}, it is recommended that 12 
            divides f. Otherwise, the necessary points to perform the protocol 
            will not be defined over the finite field with p^2 elements.
        F (FiniteField): The finite field with p^2 elements, created as 
            F.<a> = GF(p ** 2).
        E (EllipticCurve): The elliptic curve y^2 = x^3 + x over the finite 
            field F. It has j-invariant 1728. Used as a starting point for 
            the protocol.
        P_A (EllipticCurvePoint): First point of the base of E[ell_A^e_A]
        Q_A (EllipticCurvePoint): Second point of the base of E[ell_A^e_A]
        P_B (EllipticCurvePoint): First point of the base of E[ell_B^e_B]
        Q_B (EllipticCurvePoint): Second point of the base of E[ell_B^e_B]
    """
    def __init__(self, ell_A=2, ell_B=3, e_A=2, e_B=1, f=1, **kwargs):
        """Constructs a set of SIDH parameters.

        If the ell_A=2, ell_B=3, f=1 parameters are wanted, it is only required
        to define e_A and e_B. If no parameter is changed, p will be 11.
        
        The points P_A, Q_A, P_B, Q_B can be defined if fixing specific bases
        is required (e.g. for actual protocol implementation), otherwise they 
        are automatically computed.
        """
        self.ell_A = Integer(ell_A)
        self.ell_B = Integer(ell_B)
        self.e_A = Integer(e_A)
        self.e_B = Integer(e_B)
        self.f = Integer(f)

        self.p = (ell_A ** self.e_A) * (ell_B ** self.e_B) * f - 1
        F = GF(self.p ** 2 , names=('a',))
        (a,) = F._first_ngens(1)

        self.E = EllipticCurve(F, [1, 0])
        self.F = F

        if any(map(lambda x: x not in kwargs, ["P_A", "Q_A", "P_B", "Q_B"])):
            generators = self.E.objgens()[1]
            self.P_A, self.Q_A = map(lambda P: self.ell_B ** self.e_B * f * P, generators)
            self.P_B, self.Q_B = map(lambda P: self.ell_A ** self.e_A * f * P, generators)
        else:
            self.P_A = self.E(kwargs["P_A"])
            self.Q_A = self.E(kwargs["Q_A"])
            self.P_B = self.E(kwargs["P_B"])
            self.Q_B = self.E(kwargs["Q_B"])

    def __str__(self):
        """Returns a copy-and-paste-able string to fix all the parameters.

        The produced output is valid Sage code and may be used for SIDH
        protocol implementation.

        Returns:
            Serialized string.
        """
        return """SIDHParameters(
    ell_A={},
    ell_B={},
    e_A={}, 
    e_B={},
    f={},
    P_A=[\"{}\", \"{}\", \"{}\"],
    Q_A=[\"{}\", \"{}\", \"{}\"],
    P_B=[\"{}\", \"{}\", \"{}\"],
    Q_B=[\"{}\", \"{}\", \"{}\"]
)""".format(self.ell_A, self.ell_B, 
            self.e_A, self.e_B, self.f, 
            *self.P_A, *self.Q_A, 
            *self.P_B, *self.Q_B)


class SIDHPrivateKey():
    """
    A class to generate SIDH public-private key pairs.

    Attributes:
        params (SIDHParameters): A set of generated SIDH parameters.
        user (str): It can have either value "A" or "B", and indicates which
            part of the protocol should be performed.
        N (Integer): Torsion part of the curve where the user picks the private
            point. Could be ell_A^e_A or ell_B^e_B
        m (int): n (int): Private key components.
        R (EllipticCurvePoint): Private point in the base of E[N].
        phi (isogeny): Private isogeny from params.E with kernel <R>.
        public_key (SIDHPublicKey): Generated with the set_key method.
        psi (isogeny): Private isogeny from a public key to the shared secret.

    """
    def __init__(self, params, role="Alice"):
        """
        Args:
            params: an SIDHParameters object
            role: Role of the user, can be "Alice" (default) or "Bob".
        """
        self.params = params
        self.set_role(role)

    def set_role(self, role):
        """Defines the user either as Alice or as Bob.

        Args:
            role: A string starting with "A"(lice) or "B"(ob)
        """
        if role[0].upper() == "A":
            self.role = "A"
            self.N = self.params.ell_A ** self.params.e_A
        elif role[0].upper() == "B":
            self.role = "B"
            self.N = self.params.ell_B ** self.params.e_B

    def generate_key(self):
        """Generates a random private key (m, n) and executes set_key."""
        m, n = random_m_n(self.N)
        self.set_key(m, n)

    def set_key(self, m, n):
        """Set the private key as (m, n) and generate the public key.

        This function generates a private point R and a private isogeny phi
        from (m, n), and stores in public_key the image curve of phi and
        the auxiliary points.
        """
        self.m = m
        self.n = n

        if self.role == "A":
            self.R = m * self.params.P_A + n * self.params.Q_A
    
            self.phi = cyclic_isogeny(self.params.E, 
                                 self.R, 
                                 ell=self.params.ell_A, 
                                 e=self.params.e_A)

            auxiliary_points = (self.phi(self.params.P_B), self.phi(self.params.Q_B))
        elif self.role == "B":
            self.R = m * self.params.P_B + n * self.params.Q_B
    
            self.phi = cyclic_isogeny(self.params.E, 
                                 self.R, 
                                 ell=self.params.ell_B, 
                                 e=self.params.e_B)

            auxiliary_points = (self.phi(self.params.P_A), self.phi(self.params.Q_A))
        
        self.public_key = SIDHPublicKey(self.phi.codomain(), auxiliary_points)

    def key_agreement(self, public_key):
        """Takes a public key and performs a key exchange with the owned private key.

        Args:
            public_key: an SIDHPublicKey object

        Returns:
            The secret j-invariant.
        """
        S = self.m * public_key.auxiliary_points[0] + self.n * public_key.auxiliary_points[1]

        if self.role == "A":
            self.psi = cyclic_isogeny(
                public_key.curve,
                S,
                ell=int(self.params.ell_A),
                e=int(self.params.e_A))
        elif self.role == "B":
            self.psi = cyclic_isogeny(
                public_key.curve,
                S,
                ell=int(self.params.ell_B),
                e=int(self.params.e_B))

        return self.psi.codomain().j_invariant()


class SIDHPublicKey():
    """
    A serializable class to pass and SIDH public key.

    Attributes:
        curve (EllipticCurve):
        auxiliary_points ((phi(P), phi(Q))): The image via a secret isogeny of
            the ell^e-torsion (of the protocol peer) base points.
        F (FiniteField): Field of definition of the curve and auxiliary points.
    """
    def __init__(self, curve, auxiliary_points, F=None):
        """Initializes a (possibly serialized) public key

        If the curve is given by a list of coefficients in the form of
        strings, the finite field of definition must be pointed out,
        either as a parameter to this constructor or by using set_base_field.

        Args:
            curve: an EllipticCurve
            auxiliary_points: a pair of elliptic curve points
            F: a finite field (optional)
        """
        self.auxiliary_points = auxiliary_points
        if F:
            self.curve = curve
            self.set_base_field(F)
        else:
            self.curve = curve

    def set_base_field(self, F):
        """Define the field of definition of the curve and auxiliary poitns.

        Args:
            F: a finite field
        """
        if isinstance(self.curve, (list, tuple)):
            self.curve = EllipticCurve(F, self.curve)
            self.auxiliary_points = list(map(
                lambda P: self.curve(P),
                self.auxiliary_points
            ))

    def __str__(self):
        """Returns a copy-and-paste-able string to send the public key."""
        return """SIDHPublicKey(
    curve={},
    auxiliary_points=[
        [\"{}\", \"{}\", \"{}\"],
        [\"{}\", \"{}\", \"{}\"]
    ]
)""".format(list(map(lambda x: str(x), self.curve.a_invariants())), 
            *self.auxiliary_points[0], 
            *self.auxiliary_points[1])