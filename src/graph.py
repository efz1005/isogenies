import json
from sidh import *
from random import shuffle

class SIDHGraph:
    """
    A class to explore isogeny graphs to store them in JSON format.

    Attributes:
        params (SIDHParameters): SIDH protocol parameters
        record (bool): if True, the graph will be explored (allows for
            console-based arguments to turn off the recording)
        j_invariants (dict): dictionary with the isogeny graph's j-invariants
        isogenies (list): list of isogenies between the j-invariants
    """
    def __init__(self, params, record=True):
        self.params = params
        self.record = record

        self.j_invariants = {}
        self.isogenies = []
        self.__isogenies_track = set()

    def __push_torsion_points(self, E, queue, depth):
        """Push the torsion points of E to the queue for BFS exploration.

        Args:
            E (EllipticCurve)
            queue (list)
            depth (int)
        """
        degrees = [2, 3]
        shuffle(degrees)
        
        for ell in degrees:
            torsion_points = E(0).division_points(ell)
            shuffle(torsion_points)

            for P in torsion_points:
                if P == E(0):
                    continue
                queue.append((E, P, ell, depth + 1))

    def explore_graph(self, from_node=None, max_depth=10, max_iter=1000):
        """Explore graph from from_node.

        Perform a BFS exploration of the isogeny graph from the `from_node`
        curve. The exploration is limited in two ways: max_depth and max_iter.

        Args:
            from_node (EllipticCurvePoint)
            max_depth (int): maximum depth of exploration
            max_iter (int): maximum number of explored isogenies
        """
        if not self.record:
            return
        queue = []

        i = 0
        if from_node is None:
            E0 = params.E
        else:
            E0 = from_node

        self.__push_torsion_points(E0, queue, 0)

        while i < max_iter and len(queue) > 0:
            E, P, ell, depth = queue.pop(0)

            # Compute next isogeny and codomain curve
            E_next = E.isogeny(P).codomain()
            a = E.j_invariant()
            b = E_next.j_invariant()

            # Add j-invariant of visited curve and push new points to curve
            if str(b) not in self.j_invariants:
                self.j_invariants[str(b)] = {}

                if depth < max_depth:
                    self.__push_torsion_points(E_next, queue, depth)

            # Add isogeny, if not already stored
            if a != b \
                and (a, b, ell) not in self.__isogenies_track \
                and (b, a, ell) not in self.__isogenies_track:
                self.isogenies.append({
                    "source": str(E.j_invariant()),
                    "target": str(E_next.j_invariant()),
                    "degree": str(ell)
                })
                self.__isogenies_track.add((a,b,ell))

            E = E_next
            i += 1

    def add_isogeny(self, isogeny, degree, special, epoch=0):
        """Add a distinguisehd isogeny to the graph.

        Args:
            isogeny: isogeny between two curves
            degree (int): degree of the isogeny
            special (str): tag for the isogeny to be put in the graph
            epoch (int): tag for decoration of the graph purposes
        """
        if not self.record:
            return

        for phi in isogeny:
            self.isogenies.append({
                "source": str(phi.domain().j_invariant()),
                "target": str(phi.codomain().j_invariant()),
                "degree": str(degree),
                "special": str(special),
                "epoch": str(epoch)
            })

            self.add_node(phi.domain())
            self.add_node(phi.codomain())

    def add_node(self, E, special=None):
        """Add a distinguished node to the graph.

        Args:
            E (EllipticCurve)
            special (str): tag for the node to be put in the graph
        """
        if not self.record:
            return
            
        if hasattr(E, "j_invariant"):
            j = str(E.j_invariant())
        else:
            j = str(E)
        if j not in self.j_invariants:
            self.j_invariants[j] = {}

        self.j_invariants[j]["special"] = special

    def __str__(self):
        nodes = []
        for j in self.j_invariants:
            d = self.j_invariants[j]
            d["j"] = j
            nodes.append(d)

        return json.dumps({
            "nodes": nodes,
            "isogenies": self.isogenies
        }, indent=4)
